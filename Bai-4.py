#Cau a, Tuple t 4 phan tu
t = (1, 'python', [2, 3], (4, 5))
print ('Cau a: t = ',t)
#Câu b, Mở rộng t  4 thành 6 phần tử từ t ban đầu
(a, b, (c,d), (e,f)) = t
print('Cau b: t = ', a,b,c,d,e,f)
#Câu c: In giá trị cuối cùng của t
t = a,b,c,d,e,f
last = t[-1]
print('Cau c: Gia tri cua cua t la: ', last)
#Câu d: Thêm list [2,3] vào t
T = list(t)
T.append([2,3])
t = tuple(T)
print('Cau d: t = ', t)
#Câu f: Xóa phần từ [2,3] ra khỏi tuple t
n=6
t = t[ : n ] + t[n+1 : ]
print('Cau f: t = ', t)
#Câu g: Chuyển tupe t thành dạng danh sách
T = list(t)
print('Cau g: t dang list la T = ', T)

