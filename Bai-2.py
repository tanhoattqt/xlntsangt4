l = [23, 4.3, 4.2, 31, 'python', 1, 5.3, 9, 1.7]
print ('list vua nhap',l)
#Câu a) xóa 'python' ra khỏi list l
l.remove('python')
print ('Cau a: l= ', l)
#Câu b, sắp xếp tăng giảm
l.sort(reverse = True)
print ('Cau b: Danh sach giam dan', l)
l.sort(reverse = False)
print ('       Danh sach tang dan', l)
#Cau c
x = 4.2
print ('Cau c: a in l', x in l)
