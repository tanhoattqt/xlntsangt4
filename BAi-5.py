#Câu a: Nối các từ điểm lại với nhau
dic1={1:10, 2:20}
dic2={3:30, 4:40}
dic3={5:50,6:60}
dic4 = {}
for d in (dic1, dic2, dic3): dic4.update(d)
print('Cau a: Output: ', dic4)
print('       Write a Python script to print a dictionary where the keys are numbers between 1 and 15 (both included)')
v = dict()
for x in range(1, 16):
    v[x] = x ** 2
print('       Dict = ',v)
#Cau b:
import operator
d = {'a':1, 'b':2, 'c':3}
sorted_d = sorted(d.items(), key=operator.itemgetter(0))
print('Cau b: Output d = ', sorted_d)
#Cau c
string = 'Python is an easy language to learn'
dict = {}
for kytu in string:
    dict[kytu] = dict.get(kytu, 0) + 1
print('Cau c: Output = ',dict)