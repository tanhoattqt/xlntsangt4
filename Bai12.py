#Câu 12:
import random
import numpy
import string

tensanpham = ['ten1', 'ten2', 'ten3', 'ten4','ten5']
sanpham =  ['quần', 'áo', 'nón', 'giày','vớ']
giasanpham = [200,150,100,300,50]
print('Tên sản phẩm gồm:      ',tensanpham)
print('Các loại sản phẩm gồm: ',sanpham)
print('Giá sản phẩm gồm:      ',giasanpham)
print('\n\t\tBẢNG BÁO GIÁ\t\t\n')
for z in range (0, len(sanpham)):
    if (z==0):
        print('Loại sản phẩm 1: ',sanpham[0])
        print('Tên sản phẩm 1: ',tensanpham[0])
        print('Giá sản phẩm 1: ',giasanpham[0])
        print('\n')
    elif (z==1):
        print('Loại sản phẩm 2: ',sanpham[1])
        print('Tên sản phẩm 2: ',tensanpham[1])
        print('Giá sản phẩm 2: ',giasanpham[1])
        print('\n')
    elif (z==2):
        print('Loại sản phẩm 3: ',sanpham[2])
        print('Tên sản phẩm 3: ',tensanpham[2])
        print('Giá sản phẩm 3: ',giasanpham[2])
        print('\n')
    elif (z==3):
        print('Loại sản phẩm 4: ',sanpham[3])
        print('Tên sản phẩm 4: ',tensanpham[3])
        print('Giá sản phẩm 4: ',giasanpham[3])
        print('\n')
    else :
        print('Loại sản phẩm 5: ',sanpham[4])
        print('Tên sản phẩm 5: ',tensanpham[4])
        print('Giá sản phẩm 5: ',giasanpham[4])

print('\n\t\tTổng Giá = ',sum(giasanpham))

sapxep = sorted(giasanpham,reverse = True)
Dic = {
       300 : "giày " 
            "ten4 "
            "300",
       200 : "quần "
            "ten1 "
            "200",
       150 : "áo "
            "ten2 "
            "150",
       100 : "nón "
            "ten3 "
            "100",
        50 : "vớ"
            "ten4"
            "50"
       }
print('\nBa Sản Phẩm Hàng Đầu: ')
print()
for k in range (0,(len(sapxep))-1):
    if (k==0):
        print(("\t\t"),Dic[300])
    elif (k==1):
        print(("\t\t"),Dic[200])
    elif (k==2):
        print(("\t\t"),Dic[150])