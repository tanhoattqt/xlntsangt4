import numpy as np
#Cau a: Tao ma tran 3 x 4
R = 3
C = 4
matrix = []
print("Cau a: Nhap gia tri:")
for i in range(R):
        a = []
        for j in range(C):
            a.append(int(input()))
        matrix.append(a)
for i in range(R):
        for j in range(C):
            print(matrix[i][j], end=" ")
        print()
#Cau b: Tao ma tran tu 1 list ngau nhien
random_matrix_list = np.random.randint(1,10,size=(3,4))
print('Cau b: Ma tran tu list ngau nhien la: ')
print(random_matrix_list)